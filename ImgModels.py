# Reference : 
# https://github.com/tensorflow/models/blob/master/official/resnet/resnet_model.py

import os
import sys
import numpy as np
import tensorflow as tf
from tensorflow.contrib import slim

Img_Size = 512
Batch_Size = 128

x_pl = tf.placeholder(tf.float32, shape=[None, Img_Size, Img_Size, 1])
y_pl = tf.placeholder(tf.int32)
train_pl = tf.placeholder(tf.bool)

def batch_norm(inputs, training, decay=0.997, epsilon=1e-5):
    return slim.batch_norm(inputs=inputs, decay=decay, center=True, epsilon=epsilon, center=True, scale=True, is_trainig=training, fused=None)

def batch_module(inputs, repeat, filters, kernel_size, stride, training, is_batch_norm, activation_fn, weights_initializer, weights_regularizer, scope):
    with slim.arg_scope([slimg.conv2d, slim.batch_norm], activation_fn=activation_fn, weights_initializer=weights_initializer, weights_regularizer=weights_regularizer):
        for i in range(repeat):
            if i == 0:
                _net = slim.conv2d(inputs, filters, kernel_size, stride, activation_fn=None, scope=scope)
                _net = batch_norm(_net, training)
            else:
                _net = slim.conv2d(_net, filters, kernel_size, stride, activation_fn=None)
                _net = batch_norm(_net, training)
    return _net

# for i in range(3):
#       net = slim.conv2d(net, 256, [3, 3], scope='conv3_%d' % (i+1))
# net = slim.max_pool2d(net, [2, 2], scope='pool2')

def vgg16(inputs, num_classes, training, activation_fn=tf.nn.relu, weights_reg_value=0.0005, is_batch_norm=False):
    """vgg16 model"""
    with slim.arg_scope([slim.conv2d, slim.fully_connected],
                        activation_fn=activation_fn,
                        weights_initializer=tf.truncated_normal_initializer(0.0, 0.01),
                        weights_regularizer=slim.l2_regularizer(weights_reg_value)):
        net = slim.repeat(inputs, 2, slim.conv2d, 64, [3, 3], scope='conv1')
        net = slim.max_pool2d(net, [2, 2], scope='pool1')
        net = slim.repeat(net, 2, slim.conv2d, 128, [3, 3], scope='conv2')
        net = slim.max_pool2d(net, [2, 2], scope='pool2')
        net = slim.repeat(net, 3, slim.conv2d, 256, [3, 3], scope='conv3')
        net = slim.max_pool2d(net, [2, 2], scope='pool3')
        net = slim.repeat(net, 3, slim.conv2d, 512, [3, 3], scope='conv4')
        net = slim.max_pool2d(net, [2, 2], scope='pool4')
        net = slim.repeat(net, 3, slim.conv2d, 512, [3, 3], scope='conv5')
        net = slim.max_pool2d(net, [2, 2], scope='pool5')
        net = slim.fully_connected(net, 4096, scope='fc6')
        net = slim.dropout(net, 0.5, scope='dropout6')
        net = slim.fully_connected(net, 4096, scope='fc7')
        net = slim.dropout(net, 0.5, scope='dropout7')
        net = slim.fully_connected(net, num_classes, activation_fn=None, scope='fc8')
    return net

def loss_fn(logits, y):
    """loss function
    
    Args:
        logits ([tf Tensor]): logits by vgg16 model.
        y ([tf placeholder]): label placeholder for labels.
    
    Returns:
        loss [tf Tensor]: Loss tensor
    """
    cls_loss = tf.losses.sparse_softmax_cross_entropy(logits=logits, labels=y)
    return cls_loss

def build_train_op(loss, lr):
    optimizer = tf.train.AdamOptimizer(learning_rate=lr)
    ops = slim.learning.create_train_op(loss, optimizer)
    return ops

def predict_probs(sess, logits, new_imgs, training):
    sess.run(logits, feed_dict={x_pl:, new_imgs, train_pl: training})
    return pred_probs

## training model code
# for ...
#