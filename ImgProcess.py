import os
from glob import glob
from matplotlib import pyplot as plt
import cv2
import pickle

## 전체 이미지를 불러오기위한 경로 확인
# glob
# os
# ==> img_dir

## 불러오기
# imgs = [cv2.imread(path, cv2.COLOR_BGR2GRAY)[..., 0:1] for path in img_dir]
# labels = []

## 이미지 분석
# 전체 이미지의 흰색바탕의 수평선과 검정색 부분의 수평선과 평행 맞추기

## padding and resizing
# padding
# img_pad = cv2.copyMakeBorder(img, 0, 0, 512, 512, cv2.BORDER_CONSTANT, value=[0, 0, 0])
# resizing
# Img_Size = 512
# img_resize = cv2.resize(img_pad, (Img_Size, Img_Size))

## Training and Test split
# img_train
# lab_train
# img_test
# lab_test

## pickle로 저장
# tr_img_save_dir = "./img_train.p"
# with open(tr_img_save_dir, 'wb') as f:
#     pickle.dump(img_train, f)
# tr_lab_save_dir = "./lab_train.p"
# with open(tr_lab_save_dir, 'wb') as f:
#     pickle.dump(lab_train, f)

## pickle로 불러오기
# tr_img_dir = "./img_train.p"
# with open(tr_img_dir, 'rb') as f:
#    train_imgs = pickle.load(f)